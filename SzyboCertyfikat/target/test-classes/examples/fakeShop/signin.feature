Feature: signin

  Background:
    * url 'http://api.fakeshop-api.com'

  @ignore
  Scenario: signin

    Given path '/users/signin'
    When request     {"email":"janusz@email.com", "password":"test123"}
    And method Post
    Then status 201
    * def token = response.token
    * match response contains deep {"message": "User signed in successfully"}
    * match response.token == token


