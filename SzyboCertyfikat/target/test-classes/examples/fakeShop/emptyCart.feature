Feature: get cart

  Background:
    * url 'http://api.fakeshop-api.com'
    * def result = call read('signin.feature')
    * def token = result.token

  @ignore
  Scenario: empty cart

    Given path '/carts/emptyCart'
    And header Authorization = 'Bearer ' + token
    When method Post
    Then status 201
    * match response == {"message": "Cart is now empty"}
    * print "King Brus RUDY! karate miscz :)"




