Feature: add to cart

  Background:
    * url 'http://api.fakeshop-api.com'
    * def result = call read('signin.feature')
    * def token = result.token

  @ignore
  Scenario Outline: add to cart

    * def addProducts = read('file:target/oneCategory.json')
    * def productsIDs = karate.jsonPath(addProducts, "$..id")
    * def id = productsIDs [0]



    Given path 'carts/addToCart'
    And header Authorization = 'Bearer ' + token
    When request {"id":'#(<id>)'}
    And method Post
    Then status 201
    * match response == {"message": "Product added to cart"}


    Examples:
      |read('file:target/oneCategory.json')|