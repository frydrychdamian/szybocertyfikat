Feature: get cart

  Background:
    * url 'http://api.fakeshop-api.com'
    * def result = call read('signin.feature')
    * def token = result.token

    * def addProducts = read('file:target/oneCategory.json')
    * def productsNames = karate.jsonPath(addProducts, "$..name")
    * def productsPrices = karate.jsonPath(addProducts, "$..price")
    * def name = productsNames [0]
    * def price = productsPrices [0]

  @ignore
  Scenario: get cart

    Given path 'carts/getCart'
    And header Authorization = 'Bearer ' + token
    When method Get
    Then status 201
    * match response.cart contains deep [{"price": '#(price)',"name": '#(name)'}]


