Feature: Find all products

Background:
    * url 'http://api.fakeshop-api.com'

    @ignore
    Scenario: find all products

    Given path 'products/getAllProducts'
    When method Get
    Then status 201
    * match response.products contains deep {"price":#string,"name":#string,"description":#string,"id":#string,"category":"monitor","brand":#string,}
    * def category = karate.jsonPath(response, "$..products[?(@.category == 'monitor')]")
    And karate.write(category, 'oneCategory.json')



    
