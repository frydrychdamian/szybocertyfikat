Feature: get cart

  Background:
    * url 'http://api.fakeshop-api.com'
    * def result = call read('signin.feature')
    * def token = result.token

  @ignore
  Scenario: get cart

    Given path 'carts/getCart'
    And header Authorization = 'Bearer ' + token
    When method Get
    Then status 201
    * match response == {"message": "Cart is empty"}


